# Hello World from Symfony

## Deployment

Deploy your application on Cloud Run


## Build Docker Image

Build Docker image

```bash
docker build --platform linux/amd64 . -t gcr.io/tbrd-lab/hello-world-nuxt
```

Push Docker image to Docker Hub

```bash
docker push gcr.io/tbrd-lab/hello-world-nuxt
```

Test your Docker image

```bash
docker run -it -p 3000:3000 gcr.io/tbrd-lab/hello-world-nuxt
```

Deploy your Cloud Run service

```bash
gcloud run deploy hello-world-nuxt --image gcr.io/tbrd-lab/hello-world-nuxt --region europe-west3 --port 3000
```